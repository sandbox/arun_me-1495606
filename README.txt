#################################################################
#                                                               #
#  INSTALLATION GUIDE	 	                                #
#                                                               #
#  Direct debit (German/Austria),                               #
#  Creditcard, Creditcard 3d Secure,                            #
#  Prepayment, Invoice, Online Transfer, Telefon Payment        #
#  PCI Standard (Credit Card/Austria/German)                    #
#  Paypal 							#
#                                                               #
#  These modules are used for real time processing of           #
#  transaction data                                             #
#                                                               #
#  Released under the GNU General Public License                #
#   								#
#			 				  	#
#  This free contribution made by request.                      #
#  If you have found this script usefull a small recommendation #
#  as well as a comment on merchant form would be greatly	#
#  appreciated.							#
#  			                			#
#  Copyright (c) 2010 Novalnet AG                               #
#                                                               #
#################################################################
#					   	   		#
#  SPECIFICATION DETAILS		   	   		#
#					   	   		#
#  Created	     - Novalnet AG         	   		#
#					   	   		#
#  Drupal Version    – 7.x 					#
#					   	   		#
#  Novalnet Version  – 1.0.1					#
#  								#
#  Commerce Version  - 1.0   			   		#
#					   	   		#
#  Last Updated	     - 19th December 2011	   		#
#					   	   		#
#  Compatibility     - Drupal 7.x - commerce 1.0		#
#					   	   		#
#  Stability	     - Stable		   	   		#
#				  	   	   		#
#  Categories	     - Payment & Gateways  	   		#
#					   	   		#
#################################################################
 

How to install:
Please follow the steps to install the Payment Modules on your 
DrupalCommerce System:
-------------------------------------------------------------------
1.   Make sure that you have curl installed in your system, if not
     please install curl, for installation help 
     please visit  "http://curl.haxx.se/docs/install.html"
     If you use Ubantu or Debian you can try:
      sudo apt-get install curl php5-curl php5-mcrypt
      apachectl restart (restart Apache server)

2.  Copy the unzipped (novalnet includes) folder content into the 
    main folder of your shop (Please copy the files into the correct 
   folders as per the structure of this folder).

3.  Login to your Shop as admin on Browser:
    - go to the payment module section.
    - install (or reinstall if alread installed) the above mentioned
   or one of your required payment module, which you want to 
    provide for your shop's customers. 
    - after installing the module go on the edit mode, and enter the 
   necessary novalnet Parameters required for the payment.
	
4.  go to your shop and select an article to purchase as a customer.

5.  you will see your installed payment module(s) in action on the
    payment page.

6.  Before you make your Shop Live for the customers, please make
    sure that your payment mode at Novalnet is set to Live Mode.

7.  For instant bank transfer/online transfer (Sofortüberweisung)
    you need to enter a password in the admin area. You can get your 
    password via the admin tool admin.novalnet.de -> Stammdaten
    -> Paymentzugriffsschluessel.

   That's all. Your Shop is now Ready for Online Payment.

On Any Technical Problems, 
please contact sales@novalnet.de / 0049-89-923 068 320.

8. Note: Images Will not be displayed for Novalnet Where the payment
   is listed as the field size in the database is set to 128. the 
   payment method name is not displayed at back end and in the order history.
   if we add the image tag. so we have removed the image tag from 
   the payment listing area.
---------------------------------------------------------------------------

Important Notice for Online Transfer (Sofortüberweisung):

If you use real transaction data (bank code, bank account number, ect.)
 real transactions will be performed, even though the test 
mode is on/activated!
------------------------------------------------------------------------
If you do not have access to our admin tool "https://admin.novalnet.de"
 yet, you can perform tests nevertherless by using the 
following TEST data:

I. Parameters (Which are to be entered in the admin interface)
  Novalnet Vendor ID:        4
  Novalnet Vendor Auth-Code: JyEtHUjjbHNJwVztW6JrafIMHQvici
  Novalnet Product ID:       13
  Novalnet Tariff ID:        127
  Password: 		     a87ff679a2f3e71d9181a67b7542122c 
			     (Only for Credit Card, Credit Card PCI, 
                 Austria PCI, German PCI, Online Transfer and Paypal.)	

II. Test Bank Data / Test Credit Card Data (Frontend)
  1.  Direct Debit For Germany 
      Bank Account Number: 2411761956
      Bank Code:           30020900

  2.  Direct Debit For Austria
      Bank Account Number: 028121956
      Bank Code:           15090

  3.  Credit Card
      CC No.: 4200000000000000 (14 x 0)
      CVC:    123
      The expiry date must be any date of the future

  4.  Credit Card 3D Secure
      CC No.: 4200000000000000 (14 x 0)
      CVC:    123
      The expiry date must be any date of the future
      PW: tc70174 (If you are requested to enter a password while 
      testing)

  5.  Online Transfer (Sofortüberweisung)
      Password: a87ff679a2f3e71d9181a67b7542122c
      Bank Code (if you are asked to enter it on processing your
      test transaction): 88888888 (8 x 8)
      Every further entry is arbitrary
-----------------------------------------------------------------------------

Last updated: 2011.05.23	  
-----------------------------------------------------------------------------

REMARK:
-----------------------------------------------------------------------------
Regarding Prepayment, as the actual money transfer will be made some
 time later after the end customer makes his order, you have to use
 the merchant script (integrate it into your Web server and enter
 the link in the Novalnet Admin Tool (https://admin.novalnet.de;
 go to Angebote -> Ein Angebot auswählen -> Stammdaten -> Händlerskript
 URL), to which a response will be sent for every transaction by
 the Novalnet-Server. Here is an example:

test_mode=0&product_id=316&product=Testshop&marker=&payment_type=INVOICE_CREDIT
&tid=12112700001500028&currency=EUR&amount=2995&status=100&subs_billing=
&tariff_id=582&status_message=erfolgreich&paid_until=&signup_tid=&signup_date=
&signup_time=&login=&password=&firstname=xxx&lastname=xxx&street=xxx&
house_no=10&zip=12345&city=xxx&email=xxx@xxx.com&birthday=&country=Deutschland
&country_code=DE&tid_payment=12112700001429137&reference=&vendor_id=229 

The parameter payment_type=INVOICE_CREDIT means, that the money has been
 transferred. The parameter tid_payment is referring to the original TID
 (when the end customer ordered via shop) and the parameter tid implies the
 subsequent TID (once the end customer has transferred the money to the
 Novalnet Bank Account). You can sort the original tid by referring to 
the tid_payment it belongs to. 

CALLBACK SCRIPT:
-------------------------------------------------------------------------------
The following steps to call callback script(for Invoice and Prepayment module).

=> Copy the callback script (callback_novalnet2drupalcom.php) to root folder

Url: (siteurl)/callback_novalnet2xtcommerce.php
 
 [[ Ex: https://drupalcommerce.novalnet.de/callback_novalnet2drupalcom.php ]]

Note:
------------------------------------------------------------------------------

 For testing callback action script, kindly make the follwing changes in
 callback_novalnet2drupalcom.php
 
 1) $test = true;
 
 2) vendor = 4; #(Shop vendor ID - Ex: 4)

 3) tid_payment = a real existing Previous TID(when the customer ordered
 via shop) in the string assigned in the variable $t.; 
   $t = 'test_mode=0&product_id=316&product=Testshop&marker=&
   payment_type=INVOICE_CREDIT&tid=12306100000826565&currency=EUR&amount=100000
   &status=100&subs_billing=&tariff_id=582&status_message=erfolgreich&
   paid_until=&signup_tid=&signup_date=&signup_time=&login=&password=&
   firstname=xxx&lastname=xxx&street=xxx&house_no=10&zip=12345&city=xxx&
   email=xxx@xxx.com&birthday=&country=Deutschland&country_code=DE&
   tid_payment=12395900001211564&reference=8&vendor_id=4';
	
 4) amount = '' (in Eurocents. The amount of the order with tid_payment.);
 
 5) Set Database Configuration and email configuration in the following
	variables.

    Database:
	
    $mysqlhost = "";  // MySQL-Host angeben
    $mysqluser = "";  // MySQL-User angeben
    $mysqlpwd  = "";  // Passwort angeben
    $mysqldb   = "";  //database name
	
	
    Email:
	
    $comments = "Bestellstatus aenderung: Gutschrifteingang bei Novalnet
	am $today";
# The comment to be entered  on order history table
    $from = ''; # email from which the notice email to be sent to technic
    $reply_to = ''; # email where the reply to notice email to be sent
    $email = ''; # email adress where the notice email to be sent
    $copy_to = ''; # copy email where the notice email to be sent
    $caller_ip = getRealIpAddr();
    $subject = 'FEHLER beim Aufruf xt:commerce Novalnet Schnittstelle';
	# subject of the email
    $body  = "Sehr geehrte Damen und Herren,\n\nfolgender Aufruf der Status
	Aenderung
	fuer TID '".$aryCaptureParams['tid']."' konnte nicht erfolgreich bearbeitet
	werden!\n\nBei Fragen wenden Sie sich bitte an Novalnet\n
	\ncaller_ip = $caller_ip\n\nnovalnet/callback_novalnet2drupalcom.php";
	# body of the email

 
 6) Run the callback script

 For real time transactions, kindly do the follwing changes in 
callback_novalnet2drupalcom.php
 
 1) $test = false;

 2) vendor_id = your own vendor id;

 3) Set Database Configuration and Email Configurations 

 4) Log in as shop administrator and go to: Administer -> Store
 Administration -> Configuration -> Payment Settings -> Novalnet
 Prepayment Settings -> Uncheck Test Mode

----------------------------------------------------------------------

General Public License Agreement
----------------------------------------------------------------------

Preamble:
The following agreement governs the rights and responsibilities between you
(the "Partner")and the Novalnet AG ("Novalnet") in relation to the cost-free
 software solutions Service and Support provided by Novalnet, by connecting
 your e-commerce systems to the payment platform of Novalnet. Services, which
 Novalnet offers in accordance with a service contract to its partners, are
 not affected explicitly by this agreement. From this particular agreement
, is not explicitly affected the services Novalnet under the contract between
 the parties to the service contract partners are providing. By installing and
 using the software, you automatically confirm that you have read this freeware
 license agreement and agree with it. If you do not agree to these conditions,
 as a partner, please do not install and use the software.

License
Novalnet grants you a non-exclusive, free of charge right of usage on the
 payment modules provided by novalnet free of charge and all further modules
 Novalnet publishes elsewhere whose duration is limited to the duration of the
 service contract between the parties involved. According to the license
 agreement, you may install the software on one or more computers and use them.
 The license for the software is free. The partner agrees to the usage of the
payment modules and / or parts of modules exclusively for the Novalnet-provided
 services, mentioned under the Treaty/agreement. The partner is not entitled to 
 any technical support of any kind from Novalnet. Novalnet is therefore not
 obliged to ensure the maintenance or revision or developmentof the software.

Copyright 
All title, ownership rights and intellectual property rights to and from the
Software, as well as all copies of the software, and any related documentation,
 are the property of Novalnet (www.novalnet.de). All rights are reserved.
 Novalnet reserves legal measures in case of a breach of this Agreement.

Guarantee and Liability
The Payment modules will be explicitly made available "as they are defined".
 For the correct functioning of the payment modules and / or parts of the
 payment modules, Novalnet does not provide guarantee. Similarly Novalnet
 assumes no liability for damages and / or consequential damages, directly
 or indirectly which can be associated with the use of Novalnet cost-free
 payment modules, unless the damage is intentional or through gross
 negligence. Not part of this disclaimer agreement, is damage caused from
 injury to life or health.

Legal claims and severability clause 
The laws of the Federal Republic of Germany will be applicable. The place for
 court of law or going to court will be Munich. Should any present or future
 provision of the Agreement, in whole or in part, become invalid, for reasons
 other than the § § 305-310 of the Civil Code (BGB), the validity of the
 remaining provisions of the agreement will not be affected. The parties shall
 replace the ineffective, invalid or unenforceable provision by a valid one
 that will be void in its legal and economic substance, of the ineffective
 or not feasible provision, and also in compliance with the overall purpose 
 of the agreement. The same applies, if after the conclusion of the agreement,
 there are gaps or loopholes found in the agreement. The provision of
 § 139 BGB (severability) is totally excluded.

If you need further information, kindly contact our Technical service Team:

Novalnet AG
Tel.: +49 (0)89 9230683-20
Fax: +49(0)89 9230683-11
E-Mail: Technik@novalnet.de
